<?php
$db = new PDO('sqlite:site.sq3');
$body = $db->query('SELECT body FROM contents WHERE cid = 1;')
  ->fetch()['body'];
?>
<!doctype html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<title>Basic PHP Page</title>
<?php if ($body): ?>
<p><?php echo $body; ?></p>
<?php else: ?>
<p>Please put some text into table contents column body.</p>
<?php endif; ?>