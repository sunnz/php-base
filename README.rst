A very basic PHP site with SQLite database for testing and proof of
concept kind of things.

Do not use this for production site, this repo directly includes the
database, unless all your data are public.

The codebase should work with PHP 5.4 or higher. It is developed on
PHP 5.6.

If you have PHP 5.4 or higher, you can run this site locally from
the Terminal::

    $ php -S localhost:9999 -d display_errors=Off

The code uses PDO extension and PDO SQLITE driver to connect to SQLite
database, both should be enabled by default as of PHP 5.1.0.
